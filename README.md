# CoreMedia - workflow-server

## Ports

```
40305
40309
40380
40383
40398
40399
```

## dependent services

### database
```
workflow_server_database:
  host: mysql.cm.local
  port: 3306
  schema: cm_management
  user: cm_management
  password: cm_management
```

### content-management-server
```
workflow_server:
  cap:
    client:
      server:
        ior_url: http://cms.cm.local:40180/content-management-server/ior
```

### mongodb
```
workflow_server:
  mongodb:
    client_uri: mongodb://mongo.cm.local:27017/
    prefix: blueprint
```
